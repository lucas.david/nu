#include <driver.h++>

#include <fstream>
#include <sstream>

namespace nu {

  driver::driver(bool scan_debug, bool parse_debug) : scan_debug{scan_debug}, parse_debug{parse_debug}, location{},
    scanner{} {}

  void driver::error(nu::semantic_error const& e) {
    using std::string_literals::operator ""s;
    /* TODO Replace cout by os generic stream. */
    auto const& filename = *e.location.end.filename;
    std::cout << e.location << ": \033[31merror:\033[00m " << e.what() << '\n';
    std::cout.width(5);
    std::cout << e.location.end.line << " | ";
    if (e.location.end.filename) {
      std::ifstream ifs{filename};
      for (auto i = 1; i < e.location.end.line; ++i)
        ifs.ignore(std::numeric_limits<std::streamsize>::max(), '\n');
      std::string error_line;
      std::getline(ifs, error_line);
      std::cout << error_line << '\n';
    } else {
      /* TODO (<< getline) when unnamed*/
    }
    std::cout << "      | " << std::string(e.location.begin.column - 1, ' ') << '^'
      << std::string(e.location.end.column - e.location.begin.column - 1, '~');

    //"tetstststt\n";
    // std::cout << " | " << scanner().getline();
    // std::cout.write(" ", filename.size());
  }

  bool driver::parse_file(std::string const& filename) {
    std::ifstream in{filename};
    if (!in.good())
      return false;
    try {
      location = location_type{filename}; /* safe cast. */
      return parse_stream(in, std::cerr);
    } catch (semantic_error& e) {
      error(e);
      return false;
    }
  }

  bool driver::parse_stream(std::istream& in, std::ostream& out) {
    scanner.emplace(in, out);
    scanner.value().set_debug(scan_debug);

    parser_type parser{*this};
    // parser.set_debug_stream(out);
    // parser.set_debug_level(parse_debug);
    return static_cast<bool>(parser.parse());
  }

}