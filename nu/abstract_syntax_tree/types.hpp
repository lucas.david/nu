#ifndef NU_TYPES_HPP
#define NU_TYPES_HPP

#include <map>
#include <string>
#include <variant>
#include <vector>

namespace sta {
  // using type = std::variant<fundamental_type>;
}

namespace dyn {

  struct type {
    std::string name;
  };

  struct fundamental_type : public type {
    fundamental_type(std::string const& name, std::size_t size) : type{name}, size{size} {}

    std::size_t size;
    // std::map<std::string, int> static_methods{};
  };

  struct tuple_type {
    struct type;
    std::vector<type> types;
  };

  struct struct_type {
    std::string name;
  };

  struct enum_type {};

  struct union_type {};

}

/* basic fundamental types definitions. */
const dyn::fundamental_type nullptr_type{"nullptr_type", sizeof(void*)};
const dyn::fundamental_type void_type{"void", 0};
const dyn::fundamental_type char_type{"char", 4};
const dyn::fundamental_type integer_type{"int", 4};
const dyn::fundamental_type float_type{"float", 8};
/* explicitly sized signed integer types. */
const dyn::fundamental_type i8{"i8", 1};
const dyn::fundamental_type i16{"i16", 2};
const dyn::fundamental_type i32{"i32", 4};
const dyn::fundamental_type i64{"i64", 8};
/* explicitly sized unsigned integer types. */
const dyn::fundamental_type u8{"u8", 1};
const dyn::fundamental_type u16{"u16", 2};
const dyn::fundamental_type u32{"u32", 4};
const dyn::fundamental_type u64{"u64", 8};
/* explicitly sized floating-point types. */
const dyn::fundamental_type f32{"f32", 4};
const dyn::fundamental_type f64{"f64", 8};

/*
void init() {
  //symbols s;
  //s.types.emplace("int") = std::unique_ptr<fundamental_type>(integer_type);
}
*/

#endif