#ifndef NU_ABSTRACT_SYNTAX_TREE_EXPRESSION
#define NU_ABSTRACT_SYNTAX_TREE_EXPRESSION

#include <location.h++>
#include <errors/semantic_error.hpp>
#include <utils/demangle.h++>

#include <any>
#include <memory>

namespace nu {

  struct expression {
  public:
    using location_type = nu::location;
    using unique = std::unique_ptr<expression>;

  public:
    location_type location;

  public:
    expression() = delete;
    expression(location_type location) : location{location} {}
    virtual ~expression() = default;

    /* TODO pure */
    virtual std::any evaluate() /* TODO create a value only struct */ { return std::any{"not implemented"}; }
    virtual bool is_compile_time_constant() { return false; }

    virtual std::ostream& print_to(std::ostream& os) const = 0;
  };

  template<typename Char, typename Traits = std::char_traits<Char>>
  std::basic_ostream<Char, Traits>& operator<<(std::basic_ostream<Char, Traits>& os, nu::expression const& e)  { return e.print_to(os); }

  template<typename Char, typename Traits = std::char_traits<Char>>
  std::basic_ostream<Char, Traits>& print_to(std::basic_ostream<Char, Traits>& os, std::any const& any) {
    if (typeid(int) == any.type())
      return os << std::any_cast<int>(any);
    if (typeid(double) == any.type())
      return os << std::any_cast<double>(any);
    throw std::logic_error{"not_implemented_error"}; /* TODO support operation definitions. */
  }

}

#include <abstract_syntax_tree/expression/constant.h++>
#include <abstract_syntax_tree/expression/identifier.h++>
#include <abstract_syntax_tree/expression/unary_operator.h++>
#include <abstract_syntax_tree/expression/unary_operator/unary_minus_operator.h++>
#include <abstract_syntax_tree/expression/unary_operator/unary_plus_operator.h++>
#include <abstract_syntax_tree/expression/binary_operator.h++>
#include <abstract_syntax_tree/expression/binary_operator/addition_operator.h++>
#include <abstract_syntax_tree/expression/binary_operator/division_operator.h++>
#include <abstract_syntax_tree/expression/binary_operator/modulo_operator.h++>
#include <abstract_syntax_tree/expression/binary_operator/product_operator.h++>
#include <abstract_syntax_tree/expression/binary_operator/subtract_operator.h++>
#include <abstract_syntax_tree/expression/binary_operator/relational_operator.h++>
#include <abstract_syntax_tree/expression/ternary_conditional_operator.h++>

#endif