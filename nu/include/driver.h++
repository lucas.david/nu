#ifndef NU_DRIVER
#define NU_DRIVER

#include <parser.h++>
#include <scanner.h++>

#include <optional>
#include <string>

namespace nu {

  struct driver {
    using location_type = nu::location;
    using parser_type = nu::parser;
    using scanner_type = nu::scanner;

  public:
    driver() = default;
    driver(bool scan_debug = false, bool parse_debug = false);

    void error(semantic_error const& e);

    bool parse_file(std::string const& filename);
    bool parse_stream(std::istream& in, std::ostream& os);

  public:
    bool scan_debug;
    bool parse_debug;

    location_type location;
    std::optional<scanner_type> scanner;
  };

}

#endif