#ifndef NU_SYMBOLS_HPP
#define NU_SYMBOLS_HPP

#include "types.hpp"

#include <map>
#include <memory>
#include <string>
#include <variant>

namespace nu {

  struct symbols {
    // struct function {};

    // std::map<std::string, type> types;
    // std::map<std::string, std::unique_ptr<function>> functions;

    std::map<std::string, std::unique_ptr<symbols>> scoped_symbols;

    void import(symbols&);
  };

  struct symbol {
    std::string name;
  };

  struct variable : public symbol {};

}

#endif