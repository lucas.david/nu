%{ /* C++ Declarations */
    #include <scanner.h++>

    /* This disables inclusion of unistd.h.
     * The C++ scanner uses STL streams instead. */
    #define YY_NO_UNISTD_H
%}

/* enable c++ scanner class generation */
%option c++
/* enable batch processing, flex will gather a batch of input before
 * processing it. the manual says it is "somewhat more optimized".
 * should be disabled for cli-interpreters. */
/* %option batch */
/* enable scanner to generate debug output.
 * enablable within scanner class. */
/* %option debug */
%option yywrap noinput nounput
/* enables the use of start condition stacks */
%option stack

/* not using character classes from POSIX standard, neither from perl/Python/Vim/etc extensions
 * they are not supported. */

/* usefull classes. */
word      [a-zA-Z_]
/* TODO: uniword */
bin-digit     [01]
nonzero-digit [1-9]
digit         [0-9]
octal-digit   [0-7]
hex-digit     [a-f0-9]
space         [ \t\r\n\v\f]

/* '\r' deprecated since macOS 9 */
newline     \r?\n
whitespaces {space}+

/* TODO: may change to unicode wide words */
identifier {word}+

/* Literals */

unicode   \\u{digit}{0,4}
character \'([^\']|{unicode})\'
string    \"([^\"]|{unicode})\"


binary-literal  0b{bin-digit}('?{bin-digit})*
decimal-literal {nonzero-digit}('?{digit})*
octal-literal   0({octal-digit}('{octal-digit})?)*
hex-literal     0x{hex-digit}('?{hex-digit})*
integer-literal {binary-literal}|{octal-literal}|{decimal-literal}|{hex-literal}

float-literal   {decimal-literal}?\.{decimal-literal}

/* Arithmetic */
increment \+\+
decrement --

addition    \+
subtraction -
product     \*|⨯
division    \/
modulo      mod|%

/* Special Operations */
left_shift  <<
right_shift >>

/* Comparisons */
equal            ==
not_equal        !=
greater          >
greater_or_equal >=
lower            <
lower_or_equal   <=

/* Bitwise Arithmetic */
bitwise_negation ~
bitwise_and      &
bitwise_or       \|
bitwise_xor      \^

/* Logical Operations */
logical_negation !|not
logical_and      &&|and
logical_or       \|\||or
logical_xor      xor
/* implies  implies|=> */

/* Assignment and assigned operations */
assignment =

addition_assignment    {addition}{assignment}
subtraction_assignment {subtraction}{assignment}
product_assignment     {product}{assignment}
division_assignment    {division}{assignment}

left_shift_assignment  {left_shift}{assignment}
right_shift_assignment {right_shift}{assignment}

bitwise_negation_assignment {bitwise_negation}{assignment}
bitwise_and_assignment      {bitwise_and}{assignment}
bitwise_or_assignment       {bitwise_or}{assignment}
bitwise_xor_assignment      {bitwise_xor}{assignment}

%{
  #define YY_USER_INIT location.step();
  #define YY_USER_ACTION location.step(); location.columns(yyleng);
%}

%%

{newline}     location.lines();
{whitespaces} /* ignored */

compile-time throw std::logic_error{std::string{yytext} + " is not implemented yet."};
else     throw std::logic_error{std::string{yytext} + " is not implemented yet."};
enum     throw std::logic_error{std::string{yytext} + " is not implemented yet."};
for      throw std::logic_error{std::string{yytext} + " is not implemented yet."};
if       throw std::logic_error{std::string{yytext} + " is not implemented yet."};
loop     throw std::logic_error{std::string{yytext} + " is not implemented yet."};
override throw std::logic_error{std::string{yytext} + " is not implemented yet."};
return   throw std::logic_error{std::string{yytext} + " is not implemented yet."};
struct   throw std::logic_error{std::string{yytext} + " is not implemented yet."};
that     throw std::logic_error{std::string{yytext} + " is not implemented yet."};
this     throw std::logic_error{std::string{yytext} + " is not implemented yet."};

union    throw std::logic_error{std::string{yytext} + " is not implemented yet."};
var      throw std::logic_error{std::string{yytext} + " is not implemented yet."};
while    throw std::logic_error{std::string{yytext} + " is not implemented yet."};

false return nu::parser::make_boolean_literal(false, location);
true  return nu::parser::make_boolean_literal(true, location);

{integer-literal} return nu::parser::make_integer_literal(std::strtoull(yytext, 0, 0), location);
{float-literal}   return nu::parser::make_float_literal(std::strtod(yytext, 0), location);

{increment}  { std::cout << "increment\n"; }
{decrement}  { std::cout << "decrement\n"; }

{addition}    return nu::parser::make_addition(location);
{subtraction} return nu::parser::make_subtraction(location);
{product}     return nu::parser::make_product(location);
{division}    return nu::parser::make_division(location);
{modulo}      return nu::parser::make_modulo(location);

{left_shift}   { std::cout << "left_shift\n"; }
{right_shift}  { std::cout << "right_shift\n"; }

{equal}            return nu::parser::make_equal(location);
{not_equal}        return nu::parser::make_not_equal(location);
{greater}          return nu::parser::make_greater(location);
{greater_or_equal} return nu::parser::make_greater_or_equal(location);
{lower}            return nu::parser::make_lower(location);
{lower_or_equal}   return nu::parser::make_lower_or_equal(location);

{bitwise_negation}  std::cout << "bitwise_negation\n";
{bitwise_and}       return nu::parser::make_bitwise_and(location);
{bitwise_or}        return nu::parser::make_bitwise_or(location);
{bitwise_xor}       return nu::parser::make_bitwise_xor(location);

{logical_negation} std::cout << "negation\n";
{logical_and}      return nu::parser::make_logical_and(location);
{logical_or}       return nu::parser::make_logical_or(location);
{logical_xor}      return nu::parser::make_logical_xor(location);

{assignment} return nu::parser::make_assignment(location);

{addition_assignment}    return nu::parser::make_addition_assignment(location);
{subtraction_assignment} return nu::parser::make_subtraction_assignment(location);
{product_assignment}     return nu::parser::make_product_assignment(location);
{division_assignment}    return nu::parser::make_division_assignment(location);

{left_shift_assignment}   { std::cout << "left_shift_assignment\n"; }
{right_shift_assignment}  { std::cout << "right_shift_assignment\n"; }

{bitwise_negation_assignment}  { std::cout << "bitwise_negation_assignment\n"; }
{bitwise_and_assignment}       { std::cout << "bitwise_and_assignment\n"; }
{bitwise_or_assignment}        { std::cout << "bitwise_or_assignment\n"; }
{bitwise_xor_assignment}       { std::cout << "bitwise_xor_assignment\n"; }

{identifier} return nu::parser::make_identifier(yytext, location);

.       return nu::parser::symbol_type{yytext[0], location};

<<EOF>> return nu::parser::make_end_of_file(location);

%%

/* This implementation of yyFlexLexer::yylex() is required to fill the
 * vtable of the class yyFlexLexer. We define the scanner's main yylex
 * function via YY_DECL to reside in the nu::scanner class instead. */

#ifdef yylex
#undef yylex
#endif

int yyFlexLexer::yylex() {
    std::cerr << "using yyFlexLexer::yylex()!" << std::endl;
    return 0;
}

/* When the scanner receives an end-of-file indication from YY_INPUT, it then
 * checks the yywrap() function. If yywrap() returns false (zero), then it is
 * assumed that the function has gone ahead and set up `yyin' to point to
 * another input file, and scanning continues. If it returns true (non-zero),
 * then the scanner terminates, returning 0 to its caller. */
int yyFlexLexer::yywrap() {
    return 1;
}