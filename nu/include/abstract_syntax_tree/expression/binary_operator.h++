#ifndef NU_INCLUDE_ABSTRACT_SYNTAX_TREE_EXPRESSION_BINARY_OPERATOR
#define NU_INCLUDE_ABSTRACT_SYNTAX_TREE_EXPRESSION_BINARY_OPERATOR

#include <abstract_syntax_tree/expression.h++>

namespace nu {

  struct binary_operator : public nu::expression {
  private:
    nu::expression::unique left_operand_, right_operand_;

  protected:
    binary_operator(location_type location, nu::expression::unique left, nu::expression::unique right) :
      nu::expression{location}, left_operand_{std::move(left)}, right_operand_{std::move(right)} {}

  public:
    nu::expression& left_operand() noexcept { return left_operand_.operator*(); }
    nu::expression const& left_operand() const noexcept { return left_operand_.operator*(); }

    nu::expression& right_operand() noexcept { return right_operand_.operator*(); }
    nu::expression const& right_operand() const noexcept { return right_operand_.operator*(); }

    std::ostream& print_to(std::ostream& os) const override { return (*left_operand_).print_to(os << "("); }
  };

}

#endif
