#ifndef NU_INCLUDE_ABSTRACT_SYNTAX_TREE_EXPRESSION_BINARY_OPERATOR_SUBTRACTION_OPERATOR
#define NU_INCLUDE_ABSTRACT_SYNTAX_TREE_EXPRESSION_BINARY_OPERATOR_SUBTRACTION_OPERATOR

#include <abstract_syntax_tree/expression.h++>
#include <abstract_syntax_tree/expression/binary_operator.h++>

#include <memory>

namespace nu {

  struct subtract_operator final : public nu::binary_operator {
  public:
    using unique = std::unique_ptr<subtract_operator>;

  public:
    inline static unique make(location_type location, nu::expression::unique left_operand, nu::expression::unique right_operand) {
      return std::make_unique<subtract_operator>(location, std::move(left_operand), std::move(right_operand));
    }

  public:
    subtract_operator(location_type location, nu::expression::unique left_operand, nu::expression::unique right_operand) :
      nu::binary_operator{location, std::move(left_operand), std::move(right_operand)} {}

    std::any evaluate() override {
      auto const& left_eval = left_operand().evaluate(), right_eval = right_operand().evaluate();

      if (typeid(int) == left_eval.type()) {
        if (typeid(int) == right_eval.type())
          return std::any_cast<int>(left_eval) - std::any_cast<int>(right_eval);
        if (typeid(double) == right_eval.type())
          return std::any_cast<int>(left_eval) - std::any_cast<double>(right_eval);
      }
      if (typeid(double) == left_eval.type()) {
        if (typeid(int) == right_eval.type())
          return std::any_cast<double>(left_eval) - std::any_cast<int>(right_eval);
        if (typeid(double) == right_eval.type())
          return std::any_cast<double>(left_eval) - std::any_cast<double>(right_eval);
      }

      throw std::logic_error{"not_implemented_error"}; /* TODO support operation definitions. */
    }

    std::ostream& print_to(std::ostream& os) const override {
      return right_operand().print_to(nu::binary_operator::print_to(os) << " - ") << ")";
    }
  };

}

#endif
