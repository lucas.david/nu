#ifndef NU_INCLUDE_ABSTRACT_SYNTAX_TREE_STATEMENT_COMPOUND_STATEMENT
#define NU_INCLUDE_ABSTRACT_SYNTAX_TREE_STATEMENT_COMPOUND_STATEMENT

#include <abstract_syntax_tree/statement.h++>

#include <memory>

namespace nu {

  struct compound_statement final : public statement {
  private:

  public:
    static std::unique_ptr<compound_statement> make();

  public:
    std::string statement_type_name() const override { return "compound_statement"; }
  };

}

#endif