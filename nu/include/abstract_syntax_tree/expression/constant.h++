#ifndef NU_ABSTRACT_SYNTAX_TREE_EXPRESSION_CONSTANT
#define NU_ABSTRACT_SYNTAX_TREE_EXPRESSION_CONSTANT

#include <abstract_syntax_tree/expression.h++>

namespace nu {

  template<typename T>
  struct constant final : public nu::expression {
  public:
    using unique = std::unique_ptr<constant>;

  public:
    inline static unique make(location_type location, T value) {
      return std::make_unique<constant>(location, value);
    }

  public:
    T const value;

  public:
    constant(location_type location, T value) : nu::expression{location}, value{value} {}

    bool is_compile_time_constant() override { return true; }
    std::any evaluate() override { return std::make_any<T>(value); }

    std::ostream& print_to(std::ostream& os) const override {
      //if constexpr (!requires(T ignored) {{ os << value }; })
        //throw std::runtime_error{"not supported constant value type for printing..."};
      return os << value;
    }
  };

}

#endif