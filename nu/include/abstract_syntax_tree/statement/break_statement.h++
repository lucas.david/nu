#ifndef NU_INCLUDE_ABSTRACT_SYNTAX_TREE_STATEMENT_BREAK_STATEMENT
#define NU_INCLUDE_ABSTRACT_SYNTAX_TREE_STATEMENT_BREAK_STATEMENT

#include <abstract_syntax_tree/statement.h++>

#include <memory>
#include <string>

namespace nu {

  struct break_statement final : public statement {
  public:
    static std::unique_ptr<break_statement> make() { return std::unique_ptr<break_statement>{new break_statement{}}; }

  public:
    std::string statement_type_name() const override { return "break_statement"; }
  };

}

#endif
