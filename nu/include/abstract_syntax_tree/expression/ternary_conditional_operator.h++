#ifndef NU_INCLUDE_ABSTRACT_SYNTAX_TREE_EXPRESSION_TERNARY_CONDITIONAL_OPERATOR
#define NU_INCLUDE_ABSTRACT_SYNTAX_TREE_EXPRESSION_TERNARY_CONDITIONAL_OPERATOR

#include <abstract_syntax_tree/expression.h++>

namespace nu {

  struct ternary_conditional_operator final : public nu::expression {
  public:
    using unique = std::unique_ptr<ternary_conditional_operator>;

  public:
    inline static unique make(location_type location, nu::expression::unique condition, nu::expression::unique then,
      nu::expression::unique otherwise) {
      return std::make_unique<ternary_conditional_operator>(location, std::move(condition), std::move(then), std::move(otherwise));
    }

  private:
    nu::expression::unique condition_, then_, otherwise_;

  public:
    ternary_conditional_operator(location_type location, nu::expression::unique condition, nu::expression::unique then,
      nu::expression::unique otherwise) : nu::expression{location}, condition_{std::move(condition)}, then_{std::move(then)},
      otherwise_{std::move(otherwise)} {}

  public:
    nu::expression& condition() noexcept { return condition_.operator*(); }
    nu::expression const& condition() const noexcept { return condition_.operator*(); }

    nu::expression& then() noexcept { return then_.operator*(); }
    nu::expression const& then() const noexcept { return then_.operator*(); }

    nu::expression& otherwise() noexcept { return otherwise_.operator*(); }
    nu::expression const& otherwise() const noexcept { return otherwise_.operator*(); }

    std::any evaluate() override {
      auto const& condition_eval = condition().evaluate();
      if (condition_eval.type() != typeid(bool))
        throw semantic_error("cannot convert '" + demangle(condition_eval.type().name()) + "' to 'bool'", location);

      auto const& then_eval = then().evaluate(); /* TODO improve and perform type checking without evaluation. */
      auto const& otherwise_eval = otherwise().evaluate(); /* To make it lazy. */
      if (then_eval.type() != otherwise_eval.type())
        throw semantic_error("operand to ternary conditional have different types '" + demangle(then_eval.type().name())
            + "' and '" + demangle(otherwise_eval.type().name()) + "'",
          location);

      return std::any_cast<bool>(condition_eval) ? then_eval : otherwise_eval;
    }

    std::ostream& print_to(std::ostream& os) const override {
      return otherwise().print_to(then().print_to(condition().print_to(os << "(") << " ? ") << " : ") << ")";
    }

  };

}

#endif