#ifndef NU_LOCATION_HPP
#define NU_LOCATION_HPP

/**
 * \file nu/location.hpp
 * Define the nu::location class.
 */

#include <iostream>
#include <optional>
#include <string>

namespace nu {

  /** A point in a source file. */
  class position {
  public:
    using counter_type = int;

  public:
    position(counter_type line = 1, counter_type column = 1) : filename{nullptr}, line{line}, column{column} {}

    /** Construct a position. */
    position(std::string const& filename, counter_type line = 1, counter_type column = 1) : filename{&filename},
      line{line}, column{column} {}

    /** Advance to the {@code count} next lines. */
    void lines(counter_type count = 1) {
      if (count) {
        column = 1;
        line = std::max(line + count, 1);
      }
    }

    /** Advance to the {@code count} next columns. */
    void columns(counter_type count = 1) { column = std::max(column + count, 1); }

    /** File name to which this position refers. */
    std::string const* filename;
    /** Current line number. */
    counter_type line, column;
  };

  /** Add {@code width} columns to {@code result}. */
  inline position& operator+=(position& result, position::counter_type width) {
    result.columns(width);
    return result;
  }

  /** Subtract {@code width} columns to {@code result}. */
  inline position& operator-=(position& result, position::counter_type width) { return result += -width; }

  /** Return addition of {@code width} columns to {@code lhs}. */
  inline position operator+(position lhs, position::counter_type width) { return lhs += width; }

  /** Return addition of {@code width} columns to {@code lhs}. */
  inline position operator-(position lhs, position::counter_type width) { return lhs -= width; }

  /**
   * Compare two positions.
   * @return
   */
  inline bool operator==(position const& lhs, position const& rhs) {
    return lhs.line == rhs.line and lhs.column == rhs.column
      and (lhs.filename == rhs.filename or (lhs.filename and rhs.filename and *lhs.filename == *rhs.filename));
  }

  /**
   * Compare two positions.
   * @return
   */
  inline bool operator!=(position const& lhs, position const& rhs) { return !(lhs == rhs); }

  /** \brief Intercept output stream redirection.
   ** \param os the destination output stream
   ** \param p a reference to the position to redirect
   */
  template<typename CharType>
  std::basic_ostream<CharType>& operator<<(std::basic_ostream<CharType>& os, position const& p) {
    if (p.filename)
      os << '<' << *p.filename << ">:";
    return os << p.line << '.' << p.column;
  }

  /** Two points in a source file. */
  class location {
  public:
    using counter_type = position::counter_type;

  public:
    /** Construct a location from {@code begin} to {@code end}. */
    location(position const& begin, position const& end) : begin(begin), end(end) {}

    /** Construct a 0-width location at position {@code p}. */
    location(position const& p = position()) : begin(p), end(p) {}

    /** Construct a 0-width location within given {@code filename} and {@code line},
     * {@code column} position. */
    location(std::string const& filename, counter_type line = 1, counter_type column = 1) : begin(filename, line,
      column), end(filename, line, column) {}

  public:
    /** Reset initial location to final location. */
    void step() { begin = end; }

    /** Extend the current location to the {@code count} next columns. */
    void columns(counter_type count = 1) { end += count; }

    /** Extend the current location to the {@code count} next lines. */
    void lines(counter_type count = 1) { end.lines(count); }

  public:
    /** Beginning and ending of the located region. */
    position begin, end;
  };

  /** Join two locations. */
  inline location& operator+=(location& result, location const& rhs) {
    result.end = rhs.end;
    return result;
  }

  /** Add {@code width} columns to the end position of {@code result}. */
  inline location& operator+=(location& result, location::counter_type width) {
    result.columns(width);
    return result;
  }

  /** Subtract {@code width} columns to the end position of {@code result}. */
  inline location& operator-=(location& result, location::counter_type width) { return result += -width; }

  /** Join two locations. */
  inline location operator+(location lhs, location const& end) { return lhs += end; }

  /** Add {@code width} columns to the end position of {@code lhs}. */
  inline location operator+(location lhs, location::counter_type width) { return lhs += width; }

  /** Subtract {@code width} columns to the end position of {@code lhs}. */
  inline location operator-(location lhs, location::counter_type width) { return lhs -= width; }

  /**
   * Compares two locations.
   * @return true if locations are equals, false otherwise.
   */
  inline bool operator==(location const& lhs, location const& rhs) {
    return lhs.begin == rhs.begin && lhs.end == rhs.end;
  }

  /**
   * Compares two locations.
   * @return true if locations are not equals, false otherwise.
   */
  inline bool operator!=(location const& lhs, location const& rhs) { return !(lhs == rhs); }

  /**
   * Convenient redirection to output stream.
   * @param os destination output stream
   * @param l a reference to the location to redirect
   * @return output stream
   */
  template<typename CharType>
  std::basic_ostream<CharType>& operator<<(std::basic_ostream<CharType>& os, const location& l) {
    location::counter_type end_column = 0 < l.end.column ? l.end.column - 1 : 0;
    os << l.begin;
    if (l.end.filename && (!l.begin.filename || *l.begin.filename != *l.end.filename))
      os << '-' << l.end.filename << ':' << l.end.line << '.' << end_column;
    else if (l.begin.line < l.end.line)
      os << '-' << l.end.line << '.' << end_column;
    else if (l.begin.column < end_column)
      os << '-' << end_column;
    return os;
  }

}

#endif