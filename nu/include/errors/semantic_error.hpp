#ifndef NU_SEMANTIC_ERROR
#define NU_SEMANTIC_ERROR

#include <location.h++>

#include <exception>
#include <string>

namespace nu {

  struct semantic_error : std::runtime_error {
    using location_type = nu::location;

    semantic_error(std::string const& message, location_type const& location) :
      std::runtime_error{message}, location{location} {}

    location_type location;
  };

}

#endif