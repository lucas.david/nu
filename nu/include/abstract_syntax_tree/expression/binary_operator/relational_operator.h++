#ifndef NU_INCLUDE_ABSTRACT_SYNTAX_TREE_EXPRESSION_BINARY_OPERATOR_RELATIONAL_OPERATOR
#define NU_INCLUDE_ABSTRACT_SYNTAX_TREE_EXPRESSION_BINARY_OPERATOR_RELATIONAL_OPERATOR

#include <abstract_syntax_tree/expression.h++>
#include <abstract_syntax_tree/expression/binary_operator.h++>

#include <memory>

namespace nu {

  struct greater_operator final : public nu::binary_operator {
  public:
    using unique = std::unique_ptr<greater_operator>;

  public:
    inline static unique make(location_type location, nu::expression::unique left_operand, nu::expression::unique right_operand) {
      return std::make_unique<greater_operator>(location, std::move(left_operand), std::move(right_operand));
    }

  public:
    greater_operator(location_type location, nu::expression::unique left_operand, nu::expression::unique right_operand) :
      nu::binary_operator{location, std::move(left_operand), std::move(right_operand)} {}
  };

  struct greater_or_equal_operator final : public nu::binary_operator {
  public:
    using unique = std::unique_ptr<greater_or_equal_operator>;

  public:
    inline static unique make(location_type location, nu::expression::unique left_operand, nu::expression::unique right_operand) {
      return std::make_unique<greater_or_equal_operator>(location, std::move(left_operand), std::move(right_operand));
    }

  public:
    greater_or_equal_operator(location_type location, nu::expression::unique left_operand, nu::expression::unique right_operand) :
      nu::binary_operator{location, std::move(left_operand), std::move(right_operand)} {}
  };

  struct lower_operator final : public nu::binary_operator {
  public:
    using unique = std::unique_ptr<lower_operator>;

  public:
    inline static unique make(location_type location, nu::expression::unique left_operand, nu::expression::unique right_operand) {
      return std::make_unique<lower_operator>(location, std::move(left_operand), std::move(right_operand));
    }

  public:
    lower_operator(location_type location, nu::expression::unique left_operand, nu::expression::unique right_operand) :
      nu::binary_operator{location, std::move(left_operand), std::move(right_operand)} {}
  };

  struct lower_or_equal_operator final : public nu::binary_operator {
  public:
    using unique = std::unique_ptr<lower_or_equal_operator>;

  public:
    inline static unique make(location_type location, nu::expression::unique left_operand, nu::expression::unique right_operand) {
      return std::make_unique<lower_or_equal_operator>(location, std::move(left_operand), std::move(right_operand));
    }

  public:
    lower_or_equal_operator(location_type location, nu::expression::unique left_operand, nu::expression::unique right_operand) :
      nu::binary_operator{location, std::move(left_operand), std::move(right_operand)} {}
  };

  struct equal_operator final : public nu::binary_operator {
  public:
    using unique = std::unique_ptr<equal_operator>;

  public:
    inline static unique make(location_type location, nu::expression::unique left_operand, nu::expression::unique right_operand) {
      return std::make_unique<equal_operator>(location, std::move(left_operand), std::move(right_operand));
    }

  public:
    equal_operator(location_type location, nu::expression::unique left_operand, nu::expression::unique right_operand) :
      nu::binary_operator{location, std::move(left_operand), std::move(right_operand)} {}
  };

  struct not_equal_operator final : public nu::binary_operator {
  public:
    using unique = std::unique_ptr<not_equal_operator>;

  public:
    inline static unique make(location_type location, nu::expression::unique left_operand, nu::expression::unique right_operand) {
      return std::make_unique<not_equal_operator>(location, std::move(left_operand), std::move(right_operand));
    }

  public:
    not_equal_operator(location_type location, nu::expression::unique left_operand, nu::expression::unique right_operand) :
      nu::binary_operator{location, std::move(left_operand), std::move(right_operand)} {}
  };

}

#endif