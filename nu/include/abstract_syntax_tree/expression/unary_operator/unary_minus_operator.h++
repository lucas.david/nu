#ifndef NU_INCLUDE_ABSTRACT_SYNTAX_TREE_EXPRESSION_UNARY_OPERATOR_UNARY_MINUS_OPERATOR
#define NU_INCLUDE_ABSTRACT_SYNTAX_TREE_EXPRESSION_UNARY_OPERATOR_UNARY_MINUS_OPERATOR

#include <abstract_syntax_tree/expression.h++>
#include <abstract_syntax_tree/expression/unary_operator.h++>

#include <memory>

namespace nu {

  struct unary_minus_operator final : public nu::unary_operator {
  public:
    using unique = std::unique_ptr<unary_minus_operator>;

  public:
    inline static unique make(location_type location, nu::expression::unique operand) {
      return std::make_unique<unary_minus_operator>(location, std::move(operand));
    }

    unary_minus_operator(location_type location, nu::expression::unique operand) : nu::unary_operator{location, std::move(operand)} {}

    std::any evaluate() override {
      auto const& operand_eval = operand().evaluate();
      if (typeid(int) == operand_eval.type())
        return -std::any_cast<int>(operand_eval);
      if (typeid(double) == operand_eval.type())
        return -std::any_cast<double>(operand_eval);
      throw std::logic_error{"not_implemented_error"}; /* TODO support operation definitions. */
    }

    std::ostream& print_to(std::ostream& os) const override { return nu::unary_operator::print_to(os << "-"); }
  };

}

#endif
