#ifndef NU_DIVISION_BY_ZERO_HPP
#define NU_DIVISION_BY_ZERO_HPP

#include <stdexcept>

struct division_by_zero : public std::runtime_error {};

#endif