#ifndef NU_VARIABLE_HPP
#define NU_VARIABLE_HPP

#include <any>
#include <string>

struct variable {

  std::string type;
  std::string name;
  std::any value;
};

#endif