#ifndef NU_INCLUDE_ABSTRACT_SYNTAX_TREE_STATEMENT_IF_STATEMENT
#define NU_INCLUDE_ABSTRACT_SYNTAX_TREE_STATEMENT_IF_STATEMENT

#include <abstract_syntax_tree/expression.h++>
#include <abstract_syntax_tree/statement.h++>

#include <memory>
#include <string>

namespace nu {

  struct if_statement final : public statement {
  private:
    expression::unique condition_;
    statement::unique_pointer then_, otherwise_;

  public:
    static std::unique_ptr<if_statement> make(location_type location, expression::unique condition,
      statement::unique_pointer then, statement::unique_pointer otherwise = nullptr) {
      return std::make_unique<if_statement>(location, std::move(condition), std::move(then), std::move(otherwise));
    }

  public:
    if_statement(location_type location, expression::unique condition, statement::unique_pointer then,
      statement::unique_pointer otherwise = nullptr) :
      statement{location},
      condition_{std::move(condition)}, then_{std::move(then)}, otherwise_{std::move(otherwise)} {}

  public:
    std::string statement_type_name() const override { return "if_statement"; }

    expression& condition() { return condition_.operator*(); }

    expression& condition() const { return condition_.operator*(); }

    statement& then() { return then_.operator*(); }

    statement& then() const { return then_.operator*(); }

    statement& otherwise() { return otherwise_.operator*(); }

    statement& otherwise() const { return otherwise_.operator*(); }

    bool has_initialization() const { return false; } /* TODO not supported currently. */
    bool has_var_declaration() const { return false; } /* TODO not supported currently. */
    bool has_else() const { return otherwise_.operator bool(); }
  };

}

#endif