#ifndef NU_INCLUDE_ABSTRACT_SYNTAX_TREE_EXPRESSION_UNARY_OPERATOR
#define NU_INCLUDE_ABSTRACT_SYNTAX_TREE_EXPRESSION_UNARY_OPERATOR

#include <abstract_syntax_tree/expression.h++>

namespace nu {

  struct unary_operator : public nu::expression {
  private:
    nu::expression::unique operand_;

  protected:
    unary_operator(location_type location, nu::expression::unique operand) : nu::expression{location}, operand_{std::move(operand)} {}

  public:
    nu::expression& operand() noexcept { return operand_.operator*(); }
    nu::expression const& operand() const noexcept { return operand_.operator*(); }

    std::ostream& print_to(std::ostream& os) const override { return operand().print_to(os); }
  };

}

#endif