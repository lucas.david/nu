
/* require Bison 3.5 or later. */
%require "3.5"

/* enable c++ parser class generation. */
%language "c++"
%defines
/* rename namespace. */
%define api.namespace { nu }
/* rename parser class name. */

%define api.parser.class { parser }
%define api.token.constructor
/* using variant for semantic values. */
%define api.value.type variant

/* enable parser to generate debug output.  */
%debug
%define parse.assert
/* verbose error messages. */
%define parse.error verbose

/* keep track of the current position within the input. */
%locations
%define api.location.type { location }

%code requires {
  namespace nu {
    struct scanner;
  }

  #include <abstract_syntax_tree/environment.h++>
  #include <abstract_syntax_tree/expression.h++>
  #include <location.h++>
}

%param { struct driver& driver }

%code {
  #include <driver.h++>
  #include <scanner.h++>

  /* declaration of yylex function used in parser::parse() */
  namespace nu {
    static parser::symbol_type yylex(driver& driver);
  }
}

%token end_of_file 0
%token end_of_line

%token <bool>        boolean_literal
%token <int>         integer_literal
%token <double>      float_literal
%token <std::string> identifier

/* keywords */
// %token break continue do for if loop match override return struct throw var while yield

%nterm <expression::unique> expression
%nterm <statement::unique> statement
%nterm <std::vector<statement::unique>> statements

/* min priority */
%right '?' ':' assignment addition_assignment subtraction_assignment product_assignment division_assignment modulo_assignment
%left logical_or
%left logical_xor
%left logical_and
%left bitwise_or
%left bitwise_xor
%left bitwise_and
%left equal not_equal
%left greater greater_or_equal lower lower_or_equal
%left addition subtraction
%left product division modulo
%right unary_plus unary_minus
/* max priority */


%printer { $$->print(yyo); } <nu::expression::unique>;
%printer { yyo << $$; } <int> <double>;

%start program
%%

program: instruction end_of_file { }

instruction: expression ';' { nu::print_to(std::cout << *$expression << " = ", $expression->evaluate()) << "\n"; }

/* Expressions */
expression[result]:
  unary_plus expression[operand]  { $result = nu::unary_plus_operator::make(@$, std::move($operand)); }
| unary_minus expression[operand] { $result = nu::unary_minus_operator::make(@$, std::move($operand)); }
| expression[left] product expression[right]     { $result = nu::product_operator::make(@$, std::move($left), std::move($right)); }
| expression[left] division expression[right]    { $result = nu::division_operator::make(@$, std::move($left), std::move($right)); }
| expression[left] modulo expression[right]      { $result = nu::modulo_operator::make(@$, std::move($left), std::move($right)); }
| expression[left] addition expression[right]    { $result = nu::addition_operator::make(@$, std::move($left), std::move($right)); }
| expression[left] subtraction expression[right] { $result = nu::subtract_operator::make(@$, std::move($left), std::move($right)); }
| expression[condition] '?' expression[then] ':' expression[otherwise] {
    $result = nu::ternary_conditional_operator::make(@$, std::move($condition), std::move($then), std::move($otherwise));
}
| expression[left] equal expression[right]            { $result = nu::equal_operator::make(@$, std::move($left), std::move($right)); }
| expression[left] not_equal expression[right]        { $result = nu::not_equal_operator::make(@$, std::move($left), std::move($right)); }
| expression[left] greater expression[right]          { $result = nu::greater_operator::make(@$, std::move($left), std::move($right)); }
| expression[left] greater_or_equal expression[right] { $result = nu::greater_or_equal_operator::make(@$, std::move($left), std::move($right)); }
| expression[left] lower expression[right]            { $result = nu::lower_operator::make(@$, std::move($left), std::move($right)); }
| expression[left] lower_or_equal expression[right]   { $result = nu::lower_or_equal_operator::make(@$, std::move($left), std::move($right)); }
| boolean_literal { $result = nu::constant<bool>::make(@$, $boolean_literal); }
| integer_literal { $result = nu::constant<int>::make(@$, $integer_literal); }
| float_literal   { $result = nu::constant<double>::make(@$, $float_literal); }
| identifier      { $result = nu::identifier::make(@$, $identifier); }
| '(' expression[operand] ')' { $result = std::move($operand); }
;

/* Statements */
statement:
  expression-statement {}
| compound-statement   {}
| selection-statement  {}
| iteration_statement  {}
| flow-statement       {}
| try_catch_statement  {}

expression-statement: expression ';' { }

compound-statement: '{' statement-sequence '}'

statement-sequence:
  statement
| statement-sequence statement

selection-statement:
  "if" '(' expression ')' statement
  "if" '(' expression ')' statement "else" statement
  "match" '(' expression ')' statement

/* Iteration statement. */
iteration_statement:
  "while" '(' expression ')' statement
| "do" statement "while" '(' expression ')'
| "for" '(' expression ';' expression ')' statement
| "for" '(' for-range-declaration ':' expression ')' statement
| "loop"

for-range-declaration:

/* Jump statement. */
flow-statement: "break" ';'
| "continue" ';'
| "return" expression ';'
| "throw" expression ';'
| "yield" expression ';'

/* try_catch_statement */
try_catch_statement:

variable_declaration: identifier[type_name] identifier[variable_name] assignment expression[value] {
  driver.environment.add_variable($type_name, $variable_name, $value);
}
| identifier identifier
| "var" identifier assignment expression

%%

namespace nu {

  parser::symbol_type yylex(driver& driver) {
    return (*driver.scanner).yylex(driver.location);
  }

  void parser::error(location_type const& location, std::string const& message) {
    std::cout << std::string(7, ' ') << (*driver.scanner).getline() << '\n';
    std::cout << std::string(6 + location.begin.column, ' ');
    for (auto i = location.begin.column; i < location.end.column; ++i)
    std::cout << '^';
    std::cout << '\n';
    std::cout << "\033[31merror:\033[00m " << location << ": " << message << '\n';
  }

}