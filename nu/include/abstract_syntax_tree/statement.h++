#ifndef NU_ABSTRACT_SYNTAX_TREE_STATEMENT
#define NU_ABSTRACT_SYNTAX_TREE_STATEMENT

#include <location.h++>

#include <memory>
#include <string>
#include <vector>

namespace nu {

  struct statement {
  public:
    using unique_pointer = std::unique_ptr<statement>;
    using location_type = nu::location;
    using iterator = statement*;
    using const_iterator = statement const*;

  public:
    location_type const location;

  public:
    statement(location_type location) : location{location} {}

  public:
    virtual std::string statement_type_name() const = 0;

    // std::vector < public : void dump() const;
  };
  /*
    struct statement_iterator :
      std::iterator<std::forward_iterator_tag, statement, std::ptrdiff_t, statement*, statement&> {
      explicit statement_iterator(statement&&) {

      }


    };

    struct const_statement_iterator {};
  */
}

#endif
