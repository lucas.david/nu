#ifndef NU_ABSTRACT_SYNTAX_TREE_EXPRESSION_IDENTIFIER
#define NU_ABSTRACT_SYNTAX_TREE_EXPRESSION_IDENTIFIER

#include <abstract_syntax_tree/expression.h++>

namespace nu {

  struct identifier final : public nu::expression {
  public:
    using unique = std::unique_ptr<identifier>;

  public:
    inline static unique make(location_type location, std::string const& name) {
      return std::make_unique<identifier>(location, name);
    }

  public:
    std::string const name;

  public:
    identifier(location_type location, std::string name) : nu::expression{location}, name{name} {}

    std::ostream& print_to(std::ostream& os) const override { return os << name; }
  };


}

#endif