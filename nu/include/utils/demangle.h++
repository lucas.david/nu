#ifndef NU_INCLUDE_UTILS_DEMANGLE
#define NU_INCLUDE_UTILS_DEMANGLE

#include <cxxabi.h>
#include <string>

inline std::string demangle(std::string const& mangled_name) {
  std::string demangled_name;
  __cxxabiv1::__cxa_demangle(mangled_name.c_str(), demangled_name.data(), nullptr, nullptr);
  return demangled_name;
}

#endif