#ifndef NU_INCLUDE_ABSTRACT_SYNTAX_TREE_EXPRESSION_BINARY_OPERATOR_MODULO_OPERATOR
#define NU_INCLUDE_ABSTRACT_SYNTAX_TREE_EXPRESSION_BINARY_OPERATOR_MODULO_OPERATOR

#include <abstract_syntax_tree/expression.h++>
#include <abstract_syntax_tree/expression/binary_operator.h++>

#include <memory>

namespace nu {

  struct modulo_operator final : public nu::binary_operator {
  public:
    using unique = std::unique_ptr<modulo_operator>;

  public:
    inline static unique make(location_type location, nu::expression::unique left_operand, nu::expression::unique right_operand) {
      return std::make_unique<modulo_operator>(location, std::move(left_operand), std::move(right_operand));
    }

  public:
    modulo_operator(location_type location, nu::expression::unique left_operand, nu::expression::unique right_operand) :
      nu::binary_operator{location, std::move(left_operand), std::move(right_operand)} {}

    std::ostream& print_to(std::ostream& os) const override {
      return right_operand().print_to(nu::binary_operator::print_to(os) << " % ") << ")";
    }
  };

}

#endif