#ifndef NU_INCLUDE_ABSTRACT_SYNTAX_TREE_NULL_STATEMENT
#define NU_INCLUDE_ABSTRACT_SYNTAX_TREE_NULL_STATEMENT

#include <abstract_syntax_tree/statement.h++>

#include <memory>

namespace nu {

  struct null_statement final : public statement {
  private:

  public:
    static std::unique_ptr<null_statement> make();

  };

}


#endif
